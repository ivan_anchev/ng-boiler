/**
 * Set your token key here, jwt gets set in local storage under that key
**/
export const TOKEN_KEY = 'access_token';
/**
 * Tokken getter for JwtModule config
 * @see AppModule
**/
export const tokenGetter = () => localStorage.getItem(TOKEN_KEY);

/**
 * URL endpoints used for authentication
 */
export const AUTH_URLS = {
  LOGIN: 'login',
  SESSION: 'api/session'
};

/**
 * Auth scheme for Authorization header
 * @example 'Bearer '
 */
export const AUTH_SCHEME = '';

/**
 * List endpoints which are allowed to receive Authorization token
 */
export const AUTHORIZATION_ENDPOINTS = [
  'api/session'
];
