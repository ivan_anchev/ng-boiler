import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { CheckSession } from './store/actions/auth.actions';
import { AppState } from './store/app.state';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  title = 'boiler';
  constructor(private _store: Store<AppState> ) { }

  ngOnInit() {
    this._store.dispatch(new CheckSession);
  }

}
