import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { AuthService, LoginResponse } from '../../services/auth/auth.service';
import { Observable, of } from 'rxjs';
import { map, switchMap, tap, catchError} from 'rxjs/operators';

import {
  AuthActionTypes,
  LogIn, LogInSuccess, LogInFail,
  CheckSessionSuccess, CheckSessionFail } from '../actions/auth.actions';
// config
import { TOKEN_KEY } from '../../../config/auth';

@Injectable()
export class AuthEffects {

  constructor(
    private _actions: Actions,
    private _authService: AuthService) { }

  @Effect()
  checkSession$: Observable<any> = this._actions.pipe(
    ofType(AuthActionTypes.CHECK_SESSION),
    switchMap(() =>
      this._authService.checkSession().pipe(
        map((sessionData: LoginResponse) => new CheckSessionSuccess({ user: sessionData })),
        catchError(() => of(new CheckSessionFail))
      ))
  );

  @Effect()
  login$: Observable<any> = this._actions.pipe(
    ofType(AuthActionTypes.LOGIN),
    map((action: LogIn) => action.payload),
    switchMap(({ username, password }) =>
      this._authService.login(username, password)
        .pipe(
          map((loginData: LoginResponse) => new LogInSuccess({ ...loginData })),
          catchError(error => of(new LogInFail({ error })))
        )
    )
  );

  @Effect({ dispatch: false })
  loginSuccess: Observable<LoginResponse> = this._actions.pipe(
    ofType(AuthActionTypes.LOGIN_SUCCESS),
    map((action: LogInSuccess) => action.payload),
    tap(({ jwt }) => localStorage.setItem(TOKEN_KEY, jwt))
  );

  @Effect({ dispatch: false })
  logout$: Observable<any> = this._actions.pipe(
    ofType(AuthActionTypes.LOGOUT),
    tap(() => localStorage.removeItem(TOKEN_KEY))
  );
}
