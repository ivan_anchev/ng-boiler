import { Action } from '@ngrx/store';
import { LoginResponse } from '../../services/auth/auth.service';
import { User } from '../../models/user';

export enum AuthActionTypes {
  // Login/Logout
  LOGIN = '[Auth] Login',
  LOGIN_SUCCESS = '[Auth] Login Success',
  LOGIN_FAIL = '[Auth] Login Fail',
  LOGOUT = '[Auth] Logout',
  // Session check
  CHECK_SESSION = '[Auth] Check Session',
  CHECK_SESSION_SUCCESS = '[Auth] Check Session Success',
  CHECK_SESSION_FAIL = '[Auth] Check Session Fail'
}

export class Logout implements Action {
  readonly type = AuthActionTypes.LOGOUT;
}

export class LogIn implements Action {
  readonly type = AuthActionTypes.LOGIN;
  constructor(public payload: { username, password }) { }
}

export class LogInSuccess implements Action {
  readonly type = AuthActionTypes.LOGIN_SUCCESS;
  constructor(public payload: LoginResponse) { }
}

export class LogInFail implements Action {
  readonly type = AuthActionTypes.LOGIN_FAIL;
  constructor(public payload: { error: string }) { }
}

export class CheckSession implements Action {
  readonly type = AuthActionTypes.CHECK_SESSION;
}

export class CheckSessionSuccess implements Action {
  readonly type = AuthActionTypes.CHECK_SESSION_SUCCESS;
  constructor(public payload: { user: User }) { }
}

export class CheckSessionFail implements Action {
  readonly type = AuthActionTypes.CHECK_SESSION_FAIL;
}

export type All = | LogIn | LogInSuccess | LogInFail | Logout | CheckSession | CheckSessionFail | CheckSessionSuccess;
