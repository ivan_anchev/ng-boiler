import { User } from '../../models';
import { All, AuthActionTypes } from '../actions/auth.actions';

export interface AuthState {
  isAuthenticated: boolean;
  user: User | null;
  errorMessage: string | null;
  isPending: boolean;
  isCheckingSession: boolean;
  isSessionChecked: boolean;
}

export const initialState: AuthState = {
  isAuthenticated: false,
  user: null,
  errorMessage: null,
  isPending: false,
  isCheckingSession: false,
  isSessionChecked: false
};

export const reducer = (state = initialState, action: All) => {
  switch (action.type) {
    /** Login Actions **/
    case AuthActionTypes.LOGIN:
      return {
        ...state,
        isPending: true
      };

    case AuthActionTypes.LOGIN_SUCCESS:
      return {
        ...state,
        isPending: false,
        isAuthenticated: true,
        errorMessage: null,
        user: {
          username: action.payload.username,
          id: action.payload.id,
        }
      };

    case AuthActionTypes.LOGIN_FAIL:
      return {
        ...state,
        isPending: false,
        errorMessage: action.payload.error
      };

    case AuthActionTypes.LOGOUT:
      return {
        ...state,
        isAuthenticated: false,
        user: null
      };

    /** Session Actions **/
    case AuthActionTypes.CHECK_SESSION:
      return {
        ...state,
        isCheckingSession: true
      };

    case AuthActionTypes.CHECK_SESSION_SUCCESS:
      return {
        ...state,
        isCheckingSession: false,
        isSessionChecked: true,
        isAuthenticated: true,
        user: action.payload.user
      };
    case AuthActionTypes.CHECK_SESSION_FAIL:
      return {
        ...state,
        isCheckingSession: false,
        isSessionChecked: true,
        isAuthenticated: false,
        user: null
      };

    default:
      return state;
  }
};
