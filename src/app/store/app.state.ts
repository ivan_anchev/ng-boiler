import * as auth from './reducers/auth.reducers';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export interface AppState {
  auth: auth.AuthState;
}

export const reducers = {
  auth: auth.reducer
};

export const selectAuthState = createFeatureSelector<AppState, auth.AuthState>('auth');

export const selectCurrentUser = createSelector(
  selectAuthState,
  (authState: auth.AuthState) => authState.user
);

export const selectSessionReady = createSelector(
  selectAuthState,
  (authState: auth.AuthState) => {
    return authState.isSessionChecked && !authState.isCheckingSession;
  }
);
