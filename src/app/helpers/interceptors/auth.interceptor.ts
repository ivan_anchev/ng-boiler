import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { tokenGetter, AUTHORIZATION_ENDPOINTS, AUTH_SCHEME } from '../../../config/auth';
import { baseUrl } from '../../../config/common';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor() {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const { url } = req;

    /** Check if endpoint is allowed to receive Authorization token
    * @see src/config/auth
    */
    const endpoint = url.split(baseUrl)[1];
    if (endpoint && AUTHORIZATION_ENDPOINTS.indexOf(endpoint) !== -1) {

      const token = tokenGetter();
      if (token) {
        const authReq = req.clone({
          setHeaders: { Authorization: `${AUTH_SCHEME}${token}` }
        });
        return next.handle(authReq);
      }
    }

    return next.handle(req);
  }
}
