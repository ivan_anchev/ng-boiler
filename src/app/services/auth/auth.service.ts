import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { AUTH_URLS, tokenGetter } from '../../../config/auth';
import { baseUrl } from '../../../config/common';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  /**
   * Jwt helper class
   * Used for decoding token and getting expiry
   */
  private _jwtHelper = new JwtHelperService();

  constructor(private _http: HttpClient) { }

    /**
     * Check if token is expired
     * @return <boolean> Is expired
     */
    isExpired(): boolean {
      const token = tokenGetter();
      return this._jwtHelper.isTokenExpired(token);
    }

    /**
     * Get token expiration date
     * @return <Date> Expiration date
     */
    expirationDate(): Date {
      const token = tokenGetter();
      return this._jwtHelper.getTokenExpirationDate(token);
    }

  /**
   * Login user
   * Writes jwt to local storge, if successful
   *
   * @param  username Auth username
   * @param  password Auth password
   * @return <Observable<LoginResponse>> User data
   */
  login(username: string, password: string): Observable<LoginResponse> {
    const { LOGIN } = AUTH_URLS;
    const url = `${baseUrl}${LOGIN}`;

    return this._http.post<LoginResponse>(url, {
      username,
      password
    }).pipe(
      catchError(this._handleError)
    );
  }

  /**
   * Check session for currenly loged in user
   * @return Observable<LoginResponse>
   */
  checkSession(): Observable<LoginResponse> {
    const { SESSION } = AUTH_URLS;
    const url = `${baseUrl}${SESSION}`;

    if (this.isExpired()) {
      throwError('Token expired');
    }

    return this._http.get<LoginResponse>(url);
  }

  /**
   * Handle auth related Error
   *
   * @param  error Error received
   * @return <Observable<never> User-friendly error
   */
  private _handleError(error: HttpErrorResponse): Observable<never> {
    let msg = 'An error occurred';
    if (error.error instanceof ErrorEvent) {
      console.error('Error: ', error.error.message);
    } else {
      const { status } = error;

      switch (status) {
        case 401:
          msg = 'Wrong credentials!';
          break;
        case 500:
          msg = 'Server error! Please, try again';
          break;
        default:
          break;
      }
    }

    return throwError(`An error occurred: ${msg}`);
  }
}

export interface LoginResponse {
  id: number;
  username: string;
  jwt: string;
}
