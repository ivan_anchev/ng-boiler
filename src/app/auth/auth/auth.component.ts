import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState, selectAuthState, selectSessionReady, selectCurrentUser } from '../../store/app.state';
import { AuthState } from '../../store/reducers/auth.reducers';
import { Logout } from '../../store/actions/auth.actions';
import { User } from '../../models/user';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  authState$: Observable<AuthState>;

  isSessionCheckReady$: Observable<boolean>;

  user$: Observable<User>;

  constructor(private _store: Store<AppState>) { }

  ngOnInit() {
    this.authState$ = this._store.select(selectAuthState);
    this.isSessionCheckReady$ = this._store.select(selectSessionReady);
    this.user$ = this._store.select(selectCurrentUser);
  }

  _logout() {
    this._store.dispatch(new Logout);
  }
}
