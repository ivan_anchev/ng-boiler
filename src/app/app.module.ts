import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './helpers/interceptors/auth.interceptor';
import { JwtModule } from '@auth0/angular-jwt';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule, State, ActionReducer } from '@ngrx/store';
import { AuthModule } from './auth/auth.module';
import { storeLogger } from 'ngrx-store-logger';
// Components
import { AppComponent } from './app.component';
// Config
import { tokenGetter } from '../config/auth';
// Effects
import { AuthEffects } from './store/effects/auth.effects';
// Reducers
import { reducers } from './store/app.state';

const logger = (reducer: ActionReducer<State<any>>) => storeLogger()(reducer);
const metaReducers = [logger];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AuthModule,
    StoreModule.forRoot(reducers, {metaReducers}),
    EffectsModule.forRoot([ AuthEffects ])
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true,
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
